// Databrew-Email -- An email to Databrew events gateway
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew-email
//
// Databrew-Email is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew-Email is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"
import fetch from "node-fetch"
import url from "url"

import config from "./config"


// Exit status codes for system programs (used by Postfix pipe command):
// See: /usr/include/sysexits.h
const EX_OK = 0 // successful termination
const EX_NOUSER = 67 // addressee unknown
const EX_NOPERM = 77 // permission denied
const EX_SOFTWARE = 70 // internal software error

let body = Buffer(0)


async function main() {
  assert(config.apiUrl, "Missing apiUrl configuration.")
  const mailbox = process.argv[2]
  assert(mailbox, "Missing mailbox parameter.")
  if ((mailbox.match(/#/g) || []).length !== 3) {
    process.exit(EX_NOUSER)
  }
  const [namespace, dataset, eventType, eventKey] = mailbox.split("#")
  const res = await fetch(
    url.resolve(config.apiUrl, `/events/${namespace}/${dataset}/${eventType}?eventKey=${eventKey}`),
    {
      body: body.toString("binary"),
      headers: {
        // "Content-Length": new Buffer(body).length.toString(),
        "Content-Length": body.length.toString(),
        "Content-Type": "message/rfc822",
      },
      method: "POST",
    },
  )
  if (res.ok) process.exit(EX_OK)
  if (res.status === 403) process.exit(EX_NOPERM)
  if (res.status === 404) process.exit(EX_NOUSER)
  process.exit(EX_SOFTWARE)
}


process.stdin.on("readable", function () {
  const chunk = process.stdin.read()
  if (chunk !== null) body = Buffer.concat([body, chunk])
})

process.stdin.on("end", function () {
  main()
  .catch(error => console.log(error.stack))
})
